import faker from 'faker';
describe('Criar Usuarios aleatoriamente e em seguida excluir esse usuário criado', () => {
    const email = faker.internet.email();

    beforeEach(() => {
        cy.log(email)
        cy.post_criarUsuario(email)
  })
  
    it('Excluir Usuário', () => {
      cy.delete_excluirUsuario().then((response) => {
        expect(response.status).to.equal(200)
        expect(response.body).to.have.property('message', 'Registro excluído com sucesso')
      })    
    })
  })