describe('Listar Usuarios', () => {

    it('Listar usuario simples', () => {

        cy.get_listarUsuarios().then((response) => {
            expect(response.body).to.have.property('quantidade').to.be.a('number').to.satisfy(Number.isInteger);
            expect(response.body).to.have.property('usuarios').to.be.an('array').that.is.not.empty;
            expect(response.body.usuarios[0]).to.have.property('nome').to.equal('Fulano da Silva');
            expect(response.body).to.have.property('usuarios').to.be.an('array').that.is.not.empty;
            expect(response.body.usuarios.some((usuario) => usuario.administrador === 'true')).to.be.true;
        });
    })
})