import faker from 'faker';
describe('Criar Usuarios aleatoriamente e em seguida excluir esse usuário criado', () => {
    const email = faker.internet.email();
    const nomeProduto = faker.commerce.productName();
    const nomeProdutoNovo = faker.commerce.productName();

    beforeEach(() => {
        cy.post_fazerLogin()
        cy.post_cadastrarProdutoToken(nomeProduto)
  })
  
    it('Editar produto', () => {
      cy.put_editarProduto(nomeProdutoNovo).then((response) => {
        expect(response.status).to.equal(200)
        expect(response.body).to.have.property('message', 'Registro alterado com sucesso')
      })    
    })
  })