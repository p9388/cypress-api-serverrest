// pegar o token lá no .env
const accessToken = `Bearer ${Cypress.env('gitlab_access_token')}`
const userName = `${Cypress.env('user_name')}`
const userPass = `${Cypress.env('user_password')}`
//const accessToken2 = `Bearer`authorization



Cypress.Commands.add('get_listarUsuarios', () => {
    cy.request({
        method: 'GET',
        url: '/usuarios/',

    })
})

Cypress.Commands.add('post_fazerLogin', () => {
    cy.request({
        method: 'POST',
        url: '/login',
        body: {
            email: userName,
            password: userPass
        }
    }).then((response) => {
        const accessToken2 = response.body.authorization;
        // define a variável como uma propriedade do objeto cy para acessá-la em outros comandos
        cy.wrap(accessToken2).as('accessToken2');
    })
})

Cypress.Commands.add('post_criarUsuario', (email) => {
    cy.request({
        method: 'POST',
        url: '/usuarios',
        body: {
            "nome": "Caio Augusto",
            "email": email,
            "password": "teste",
            "administrador": "true"
        }
    }).then(response => {
        const id = response.body._id
        cy.log(id)
        cy.wrap(id).as('userId')
      })
})

Cypress.Commands.add('delete_excluirUsuario', () => {
    cy.get('@userId').then(userId => {
      cy.request({
        method: 'DELETE',
        url: `/usuarios/${userId}`
      })
    })
  })





Cypress.Commands.add('post_cadastrarProduto', (produto) => {
    cy.request({
        method: 'POST',
        url: '/produtos',
        body: {
            "nome": produto,
            "preco": 2000,
            "descricao": "Novo produto",
            "quantidade": 400
        },
        headers: { Authorization: accessToken }

    })
})

// Regra de negocio produto
Cypress.Commands.add('post_cadastrarProdutoToken', (produto) => {
    // usa a variável accessToken no cabeçalho Authorization
    cy.get('@accessToken2').then((accessToken2) => {
        cy.request({
            method: 'POST',
            url: '/produtos',
            body: {
                "nome": produto,
                "preco": 2000,
                "descricao": "Produto novo",
                "quantidade": 400
            },
            headers: { Authorization: accessToken2 }
        }).then(response => {
            const id = response.body._id
            cy.log(id)
            cy.wrap(id).as('userIdProduct')
          });
    });

    // Cypress.Commands.add('put_editarProduto', () => {
    //     cy.get('@accessToken2','@userIdProduct').then((accessToken2,userIdProduct) => {
    //         cy.request({
    //             method: 'PUT',
    //             url: `/produtos/${userIdProduct}`,
    //             body: {
    //                 "nome": produto,
    //                 "preco": 2000,
    //                 "descricao": "Novo produto",
    //                 "quantidade": 400
    //             },
    //             headers: { Authorization: accessToken2 }
    //         });
    //     })
    //   })


      // outro teste

      Cypress.Commands.add('put_editarProduto', (produto) => {
        cy.get('@accessToken2').then((accessToken2) => {
            cy.get('@userIdProduct').then((userIdProduct) => {
                 cy.request({
                      method: 'PUT',
                     url: `/produtos/${userIdProduct}`,
                 body: {
                     "nome": produto,
                     "preco": 1,
                        "descricao": "Novo produto",
                    "quantidade": 2
                },
                headers: { Authorization: accessToken2 }
            });
        })
      })
});

})

